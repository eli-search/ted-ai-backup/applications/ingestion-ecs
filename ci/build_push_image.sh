#!/usr/bin/env bash

image_url=$1

echo "{\"credsStore\":\"ecr-login\"}" > /kaniko/.docker/config.json

/kaniko/executor \
  --single-snapshot \
  --context "${CI_PROJECT_DIR}" \
  --dockerfile "Dockerfile" \
  --destination "$image_url" \
  --build-arg docker_base_image_url="${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:3.10-slim-buster"
